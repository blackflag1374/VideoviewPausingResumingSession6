package com.example.admin.myapplication.weather.MyTextView.MyEditTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Admin on 2/9/2018.
 */

public class MyEditTextView extends AppCompatEditText {
    private Context context;

    public MyEditTextView(Context context) {
        super(context);
        this.context = context;
        this.setTypeface();

    }

    public MyEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.setTypeface();

    }
        void setTypeface(){
            Typeface saf = Typeface.createFromAsset(context.getAssets(), "en_saf");
          this.setTypeface(saf);
    }




}
