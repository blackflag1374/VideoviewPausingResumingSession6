package com.example.admin.myapplication.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.admin.myapplication.R;

public class VisibleInVisibleActivity extends AppCompatActivity {
  ImageView img ;
    Button toggle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visible_in_visible);
        img=findViewById(R.id.img);
        findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(img.getVisibility()==View.INVISIBLE  )
                      img.setVisibility(View.VISIBLE);
                else img.setVisibility(View.INVISIBLE);




            }
        });
    }



}
