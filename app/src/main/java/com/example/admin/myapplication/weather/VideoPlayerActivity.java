package com.example.admin.myapplication.weather;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.admin.myapplication.R;

public class VideoPlayerActivity extends AppCompatActivity {
    VideoView myVideo;
    String videoURL = "https://as11.cdn.asset.aparat.com/aparat-video/80206896e968d3c1bcb75d65becc3ddd9560992-144p__92081.mp4";
    int stopPositoipn;
    BroadcastReceiver callReciver ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        myVideo = findViewById(R.id.myVideo);
        myVideo.setVideoURI(Uri.parse(videoURL));
        myVideo.setMediaController(new MediaController(this));
        myVideo.start();
        CheckPermission();

        callReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(myVideo.isPlaying())
                    myVideo.pause();
            }
        };
        IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver( callReciver, callFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReciver);
    }
    public void onPause() {
        super.onPause();
        stopPositoipn = myVideo.getCurrentPosition();
        myVideo.pause();


        }

    public void onResume(){
        super.onResume();
        myVideo.seekTo(stopPositoipn);
        myVideo.start();


    }


    private void CheckPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}
                    , 3700
            );
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 3700) ;
    }
}
