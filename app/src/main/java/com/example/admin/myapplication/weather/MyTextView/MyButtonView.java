package com.example.admin.myapplication.weather.MyTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by Admin on 2/9/2018.
 */

public class MyButtonView extends AppCompatButton {
    private Context context;

    public MyButtonView(Context context) {
        super(context);
        this.context = context;
        this.setTypeface();

    }

    public MyButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface();

    }
    void setTypeface(){
        Typeface saf = Typeface.createFromAsset(context.getAssets(), "en_saf.ttf");
        this.setTypeface(saf);
    }
}
