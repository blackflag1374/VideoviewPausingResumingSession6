package com.example.admin.myapplication.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.weather.Models.Forecast;

import java.util.List;

/**
 * Created by Admin on 2/5/2018.
 */

public class ForecastListAdapter extends BaseAdapter {
    Context mContext;
    List forecastlist;

    public ForecastListAdapter(Context mContext, List<Forecast> forecastlist) {
        this.mContext = mContext;
        this.forecastlist = forecastlist;
    }

    @Override
    public int getCount() {
        return forecastlist.size();
    }

    @Override
    public Object getItem(int i) {
        return forecastlist.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItemId(i);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        View v = LayoutInflater.from(mContext).inflate(R.layout.forecastlist_item, null);

        TextView day = v.findViewById(R.id.day);
        TextView date = v.findViewById(R.id.date);
        TextView high = v.findViewById(R.id.high);
        TextView low = v.findViewById(R.id.low);
        TextView text = v.findViewById(R.id.text);


        return null;
    }
}
